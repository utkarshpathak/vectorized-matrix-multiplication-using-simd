#include"dgemm.h"
#include <immintrin.h>
#include <inttypes.h>
//#include<omp.h>

void dgemm(float *a, float *b, float *c, int n)
{
 register int ub = n - (n % 8);
 __m256 va, vb, vc, tmp;
 float t[8];
 
    //#pragma omp parallel for num_threads(8) 
    for(register int i = 0; i < n; i++){
        for(register int j = 0; j < n; j++){
            
            tmp = _mm256_setzero_ps();    
        
		    for(register int k = 0; k < ub; k=k+8)            
            {
		         va = _mm256_loadu_ps(&a[i*n+k]);
		         vb = _mm256_loadu_ps(&b[j*n+k]);
		         vc = _mm256_mul_ps(va, vb);
		         tmp = _mm256_add_ps(tmp, vc);      
            }

			vc = _mm256_permute2f128_ps(tmp, tmp, 1);
			tmp = _mm256_add_ps(tmp, vc);
			tmp = _mm256_hadd_ps(tmp,tmp);
			tmp = _mm256_hadd_ps(tmp,tmp);
			_mm256_store_ps(t,tmp);
			c[i * n + j] += t[0];      
            
            for(register int k = ub; k < n; k++){
                c[i * n + j] += a[i * n  + k] * b[j * n  + k];
            }
            
        }
    }
}

// Explanation:

// 0-1-2-3-4-5-6-7 = tmp
// 4-5-6-7-0-1-2-3 = vc
// 0+4 - 1+5 - 2+6 - 3+7 - 0+4 - 1+5 - 2+6 - 3+7 = tmp after add
// 0+4+1+5 - 2+6+3+7 - .... = tmp after hadd
// 0+4+1+5+2+6+3+7 - ..... = tmp after hadd

// Note:

// 0-1-2-3-4-5-6-7 and 0-1-2-3-4-5-6-7
// 0+1 - 2+3 - 0+1 - 2+3 - .... this is why 3 times hadd doesn't work.

